import unittest
from flask import Response
from app import db, create_app
from app.models.products import Product, Brand, Category
import json
import datetime


brands_data = [
        {'name': 'Apple', 'country_code': 'US'},
        {'name': 'Milka', 'country_code': 'DE'}
    ]

categories_data = [
    {'name': 'Mobile phones'},
    {'name': 'Food'}
]

products_data = [
    {'name': 'iPhone', 'brand_id': 1, 'rating': 10, 'featured': True, 'items_in_stock': 10, 'receipt_date': None,
     'created_at': datetime.datetime.utcnow()},
    {'name': 'Chocolate', 'brand_id': 2, 'rating': 7, 'featured': False, 'items_in_stock': 0,
     'receipt_date': datetime.datetime.utcnow() + datetime.timedelta(days=10), 'created_at': datetime.datetime.utcnow()}
]


class Test(unittest.TestCase):
    def setUp(self) -> None:
        self.app = create_app(
            dict(
                TESTING=True,  # Propagate exceptions
                LOGIN_DISABLED=False,  # Enable @register_required
                MAIL_SUPPRESS_SEND=True,  # Disable Flask-Mail send
                SERVER_NAME='localhost',  # Enable url_for() without request context
                SQLALCHEMY_DATABASE_URI='sqlite:///:memory:',  # In-memory SQLite DB
                WTF_CSRF_ENABLED=False,  # Disable CSRF form validation
            )
        )
        self.client = self.app.test_client()
        with self.app.app_context():
            db.create_all(app=self.app)
            for brand in brands_data:
                db.session.add(Brand(**brand))
            for cat in categories_data:
                db.session.add(Category(**cat))
            db.session.commit()
            iphone = Product(**products_data[0])
            iphone.categories.append(Category.query.filter_by(name='Mobile phones').first())
            db.session.add(iphone)
            chocolate = Product(**products_data[1])
            chocolate.categories.append(Category.query.filter_by(name='Food').first())
            db.session.add(chocolate)
            db.session.commit()
        pass

    def tearDown(self) -> None:
        with self.app.app_context():
            db.drop_all()

    def test_get(self):
        """
        Добавление всех объектов
        """
        resp = self.client.get('/products')  # type: Response
        assert resp.status_code == 200

    def test_put(self):
        """
        Добавление объекта
        """
        test_object = {
            "categories": [1, 2],
            "brand": 1,
            "featured": True,
            "items_in_stock": 10,
            "name": "iPhone",
            "rating": 10.0,
        }
        with self.app.app_context():
            resp = self.client.put('/products', data=json.dumps(test_object),
                                   headers={'Content-Type': 'application/json'})
            product_id = json.loads(resp.data)['id']
            assert Product.query.filter_by(id=product_id).first()
            assert Product.query.filter_by(id=product_id).first().featured
            test_object['categories'].clear()
            resp = self.client.put('/products', data=json.dumps(test_object),
                                   headers={'Content-Type': 'application/json'})
            assert resp.status_code == 400
            test_object['brand'] = 42
            test_object['categories'] = [1]
            resp = self.client.put('/products', data=json.dumps(test_object),
                                   headers={'Content-Type': 'application/json'})
            assert resp.status_code == 400

    def test_post(self):
        """
        Изменение объекта
        """
        test_object = {
            "categories": [1],
            "brand": 1,
            "expiration_date": None,
            "featured": False,
            "items_in_stock": 10,
            "name": "iPhone XS",
            "rating": 9.0,
            "receipt_date": None
        }
        with self.app.app_context():
            resp = self.client.post('/products/1', data=json.dumps(test_object), headers={'Content-Type': 'application/json'})
            assert resp.status_code == 200
            assert Product.query.filter_by(id=1).first().name == test_object['name']
            test_object['categories'].clear()
            resp = self.client.post('/products/1', data=json.dumps(test_object),
                                    headers={'Content-Type': 'application/json'})
            assert resp.status_code == 400

    def test_delete(self):
        """
        Удаление объекта
        """
        with self.app.app_context():
            self.client.delete('/products/1')
            assert Product.query.filter_by(id=1).first() is None

    def test_expiration(self):
        """
        Проверка ограничения на дату истечение
        """
        test_object = {
            "categories": [1, 2],
            "brand": 1,
            "featured": True,
            "items_in_stock": 10,
            "name": "iPhone",
            "rating": 10.0,
            "expiration_date": "Sun, 07 Jul 2019 19:05:11 GMT"
        }
        resp = self.client.put('/products', data=json.dumps(test_object),
                               headers={'Content-Type': 'application/json'})
        assert resp.status_code == 400


if __name__ == '__main__':
    unittest.main()
