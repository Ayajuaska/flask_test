from datetime import datetime

from flask import Blueprint, jsonify
from flask.ext.restful import HTTPException
from flask_restful import reqparse

from app import db
from app.models.products import Product, Brand, Category

products_blueprint = Blueprint('products', __name__)


def dt_parser(*args):
    dt = datetime.strptime(args[0], '%a, %d %b %Y %X %Z')
    return dt


def category(value, *_):
    return Category.query.filter_by(id=value).first()


def brand(value, *_):
    if Brand.query.filter_by(id=value).first():
        return int(value)
    raise HTTPException()


def name(value, *_):
    if value and 0 < len(value) < 50:
        return value
    raise HTTPException()


parser = reqparse.RequestParser()
parser.add_argument('brand', type=brand, nullable=False, required=True)
parser.add_argument('categories', action="append", type=category, nullable=False, required=True)
parser.add_argument('expiration_date', type=dt_parser, required=False)
parser.add_argument('featured', type=bool)
parser.add_argument('items_in_stock', type=int, required=True)
parser.add_argument('name', required=True, type=name)
parser.add_argument('rating', type=float, required=True)
parser.add_argument('receipt_date', type=dt_parser)


def validate_product(product):
    exc = HTTPException()
    exc.code = 400
    if len(product.categories) < 1 or len(product.categories) > 5:
        raise exc
    if product.expiration_date and (datetime.now() - product.expiration_date).days < 30:
        raise exc
    if product.rating > 8:
        product.featured = True
    if product.items_in_stock < 1 and product.receipt_date is None:
        raise exc


@products_blueprint.route('/products', methods=['GET'])
def get_products():
    return jsonify({
        'results': [p.serialized for p in Product.query.all()]
    })


@products_blueprint.route('/products', methods=['PUT'])
def add_product():
    params = parser.parse_args()
    categories = params.pop('categories')
    brand_id = params.pop('brand')
    product = Product(**params)
    product.brand_id = brand_id
    product.categories = categories
    validate_product(product)
    db.session.add(product)
    db.session.commit()
    return jsonify({'id': product.id})


@products_blueprint.route('/products/<product_id>', methods=['GET'])
def get_product(product_id):
    return jsonify(Product.query.filter_by(id=product_id).first().serialized)


@products_blueprint.route('/products/<product_id>', methods=['POST'])
def modify_product(product_id):
    params = parser.parse_args()
    categories = params.pop('categories')
    Product.query.filter_by(id=product_id).update(params)
    product = Product.query.filter_by(id=product_id).first()
    product.categories.clear()
    product.categories += categories
    validate_product(product)
    db.session.add(product)
    db.session.commit()
    return jsonify(product.serialized)


@products_blueprint.route('/products/<product_id>', methods=['DELETE'])
def delete_product(product_id):
    return jsonify({'deleted': Product.query.filter_by(id=product_id).delete()})
